﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupMessage : Singleton<PopupMessage> {

    public GameObject container;
    public Image messageBox;
    public Text message;
    public Button confirmButtom;
    public Text buttonText;
    public Image progressFill;

    public void SetProgress(float progress) {
        progressFill.fillAmount = progress;
    }
    public void ShowPopup(bool show) {
        container.SetActive(show);
    }
}
