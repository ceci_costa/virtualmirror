﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BehaviourMirror : MonoBehaviour {

    /// <summary>
    /// Camera used to project the cursor at a specified distance or to be faced by the object
    /// </summary>
    public Camera lookAt;
    /// <summary>
    /// Object to be thacker by the cursor
    /// </summary>
	public Transform toTrack;
    /// <summary>
    /// Container for content that might be show or hide when the tracked object is active or not
    /// </summary>
	public GameObject container;
    /// <summary>
    /// Collider that might be used to trigger some event with for this cursor
    /// </summary>
    public Collider collider;
    public bool hideWhenNotDetected = false;
    public bool faceCamera = false;
    public bool useFixedCameraDeaph = false;
    public bool doNotFollow = false;
    public float deaph = 1.5f;

    private Vector3 previousDirection;
    public bool copyRotation = false;
    public bool copyScale = false;

    // Use this for initialization
    void Start() {
        previousDirection = new Vector3(0, 1, 0);
    }

    // Update is called once per frame
    void LateUpdate() {

        //Disable or enable collider according to the Tracked object active state
        if (collider != null) {
            collider.enabled = toTrack.gameObject.activeInHierarchy;
        }

        //Hide or show container according to the Tracked object active state
        if (container != null && hideWhenNotDetected) {
            container.SetActive(toTrack.gameObject.activeInHierarchy);
        }

        //Project object from the camera to world, at a specified distance
        if (!doNotFollow && useFixedCameraDeaph) {
            ProjectFromScreenToDeaph();
        } else if (!doNotFollow && toTrack.gameObject.activeInHierarchy) {
            transform.position = toTrack.position;
        }

        if (copyRotation && toTrack.gameObject.activeInHierarchy) {
            transform.rotation = toTrack.rotation;
        }

        if (copyScale && toTrack.gameObject.activeInHierarchy) {
            transform.localScale = toTrack.localScale;
        }

        //Makes the object always face the camera 
        if (faceCamera) {
            LookAt();
        }
    }

    private void ProjectFromScreenToDeaph() {
        Vector3 point = lookAt.WorldToScreenPoint(toTrack.transform.position);
        point.z = deaph;
        transform.position = lookAt.ScreenToWorldPoint(point);
    }

    void LookAt() {
        transform.LookAt(transform.position + lookAt.transform.rotation * Vector3.forward,
            lookAt.transform.rotation * Vector3.up);

        //transform.RotateAround (transform.parent.transform.position, new Vector3(0,0,1), delta);

        Quaternion q = new Quaternion();
        q.SetFromToRotation(transform.parent.transform.up, previousDirection);
        Vector3 position = transform.position - transform.parent.transform.position;
        position = q * position;
        transform.position = position + transform.parent.transform.position;
        previousDirection = transform.parent.transform.up;
    }
}