﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DownloadFromServer : Singleton<DownloadFromServer> {
    private WWW www;
    private bool done = true;
    public WWW Www {
        get {
            return www;
        }
    }

    public bool Done {
        get {
            return done;
        }
    }

    public void SetPicture(string picturePathAndName, bool isExternal, RawImage output) {
        done = false;
        if (!isExternal) {
            StartCoroutine(downloadImg("file://"+ picturePathAndName, output));
        } else {
            StartCoroutine(downloadImg(picturePathAndName, output));
        }
    }

    public void GetFile(string PathAndName) {
        done = false;
        StartCoroutine(downloadFile(PathAndName));
    }

    public float GetProgress() {
        return www.progress;
    }

    public byte[] GetBytes() {
        return www.bytes;
    }

    IEnumerator downloadFile(string url) {
        www = new WWW(url);
        yield return www;
        done = true;
    }

    IEnumerator downloadImg(string url, RawImage output) {
        Texture2D texture = new Texture2D(1, 1);
        www = new WWW(url);
        yield return www;
        www.LoadImageIntoTexture(texture);


        Vector2 imgSize = output.rectTransform.rect.size;
        //Adjust the image size to fit the thumbnail withouth overflowing the cell
        float texAspect = (float)texture.width / (float)texture.height;
        if (texAspect >= 1) {
            output.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (int)imgSize.x);
            output.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (int)(imgSize.x / texAspect));
        } else {
            output.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (int)(imgSize.y * texAspect));
            output.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (int)imgSize.y);
        }

        output.texture = texture;
        done = true;
    }

}


