﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.IO;
using System;
using System.Threading;

public class FtpDownload : Singleton<FtpDownload> {

    private bool failed = false;
    public bool Failed {
        get {
            return failed;
        }
    }

    private bool done = false;
    public bool Done {
        get {
            return done;
        }
    }

    byte[] data;
    private Thread t;

    public byte[] Bytes {
        get {
            return data;
        }
    }

    public void downloadWithFTP(string ftpUrl, string savePath = "", string userName = "", string password = "") {
        done = false;
        failed = false;
        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new System.Uri(ftpUrl));
        //request.Proxy = null;

        request.UsePassive = true;
        request.UseBinary = true;
        request.KeepAlive = true;

        //If username or password is NOT null then use Credential
        if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password)) {
            request.Credentials = new NetworkCredential(userName, password);
        }

        request.Method = WebRequestMethods.Ftp.DownloadFile;
        //If savePath is NOT null, we want to save the file to path
        //If path is null, we just want to return the file as array
        if (!string.IsNullOrEmpty(savePath)) {
            t = new System.Threading.Thread( delegate(){
                downloadAndSave(request, savePath);
            });
            t.Start();
        } else {
            t = new System.Threading.Thread(delegate () {
                downloadAsbyteArray(request);
            });
            t.Start();
        }

    }

    internal void Cancel() {
        t.Abort();
    }

    void downloadAsbyteArray(FtpWebRequest ftprequest) {
        try {
            WebResponse request = ftprequest.GetResponse();

            using (Stream input = request.GetResponseStream()) {
                byte[] buffer = new byte[16 * 1024];
                using (MemoryStream ms = new MemoryStream()) {
                    int read;
                    while (input.CanRead && (read = input.Read(buffer, 0, buffer.Length)) > 0) {
                        ms.Write(buffer, 0, read);
                    }
                    done = true;
                    data = ms.ToArray();
                }
            }
        }catch(System.Exception e) {
            Debug.Log(e.Message);
            failed = true;
        }
    }

    void downloadAndSave(FtpWebRequest ftprequest, string savePath) {

        try {
            WebResponse request = ftprequest.GetResponse();
            Stream reader = request.GetResponseStream();

            //Create Directory if it does not exist
            if (!Directory.Exists(Path.GetDirectoryName(savePath))) {
                Directory.CreateDirectory(Path.GetDirectoryName(savePath));
            }

            FileStream fileStream = new FileStream(savePath, FileMode.Create);

            int bytesRead = 0;
            byte[] buffer = new byte[2048];

            while (true) {
                bytesRead = reader.Read(buffer, 0, buffer.Length);

                if (bytesRead == 0)
                    break;

                fileStream.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();
            done = true;
        } catch (System.Exception e) {
            Debug.Log(e.Message);
            failed = true;
        }
    }
}
