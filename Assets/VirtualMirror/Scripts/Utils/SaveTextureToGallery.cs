﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveTextureToGallery {

    private const string MediaStoreImagesMediaClass = "android.provider.MediaStore$Images$Media";
    private static AndroidJavaObject _activity;

    public static string SaveImageToGallery(Texture2D texture2D, string title, string description) {
        using (var mediaClass = new AndroidJavaClass(MediaStoreImagesMediaClass)) {
            using (var cr = Activity.Call<AndroidJavaObject>("getContentResolver")) {
                var image = Texture2DToAndroidBitmap(texture2D);
                var imageUrl = mediaClass.CallStatic<string>("insertImage", cr, image, title, description);
                return imageUrl;
            }
        }
    }

    public static AndroidJavaObject Texture2DToAndroidBitmap(Texture2D texture2D) {
        byte[] encoded = texture2D.EncodeToJPG();
        Debug.Log(encoded.Length);
        using (var bf = new AndroidJavaClass("android.graphics.BitmapFactory")) {
            return bf.CallStatic<AndroidJavaObject>("decodeByteArray", encoded, 0, encoded.Length);
        }
    }

    public static AndroidJavaObject Activity {
        get {
            if (_activity == null) {
                var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                _activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            }
            return _activity;
        }
    }

    private IEnumerator TakeScreenshot() {
        yield return new WaitForEndOfFrame();

        //INITIAL SETUP
        string myFilename = "myScreenshot.png";
        string myDefaultLocation = Application.persistentDataPath + "/" + myFilename;
        //EXAMPLE OF DIRECTLY ACCESSING THE Camera FOLDER OF THE GALLERY
        //string myFolderLocation = "/storage/emulated/0/DCIM/Camera/";
        //EXAMPLE OF BACKING INTO THE Camera FOLDER OF THE GALLERY
        //string myFolderLocation = Application.persistentDataPath + "/../../../../DCIM/Camera/";
        //EXAMPLE OF DIRECTLY ACCESSING A CUSTOM FOLDER OF THE GALLERY
        string myFolderLocation = "/storage/emulated/0/DCIM/MyFolder/";
        string myScreenshotLocation = myFolderLocation + myFilename;

        //ENSURE THAT FOLDER LOCATION EXISTS
        if (!System.IO.Directory.Exists(myFolderLocation)) {
            System.IO.Directory.CreateDirectory(myFolderLocation);
        } else {
            Debug.Log(myDefaultLocation + " doesn't exists");
        }

        //TAKE THE SCREENSHOT AND AUTOMATICALLY SAVE IT TO THE DEFAULT LOCATION.
        UnityEngine.ScreenCapture.CaptureScreenshot(       myFilename);

        //MOVE THE SCREENSHOT WHERE WE WANT IT TO BE STORED
        System.IO.File.Move(myDefaultLocation, myScreenshotLocation);

        //REFRESHING THE ANDROID PHONE PHOTO GALLERY IS BEGUN
        AndroidJavaClass classPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject objActivity = classPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaClass classUri = new AndroidJavaClass("android.net.Uri");
        AndroidJavaObject objIntent = new AndroidJavaObject("android.content.Intent", new object[2] { "android.intent.action.MEDIA_MOUNTED", classUri.CallStatic<AndroidJavaObject>("parse", "file://" + myScreenshotLocation) });
        objActivity.Call("sendBroadcast", objIntent);
        //REFRESHING THE ANDROID PHONE PHOTO GALLERY IS COMPLETE

        //AUTO LAUNCH/VIEW THE SCREENSHOT IN THE PHOTO GALLERY
        Application.OpenURL(myScreenshotLocation);
        //AFTERWARDS IF YOU MANUALLY GO TO YOUR PHOTO GALLERY, 
        //YOU WILL SEE THE FOLDER WE CREATED CALLED "myFolder"
    }
}
