﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AdjustImageTouchController : MonoBehaviour {

    [System.Serializable]
    public class OnBoolValuesChange : UnityEngine.Events.UnityEvent<bool> { }


    public RectTransform handle;
    RectTransform touchArea;
    public float speed = 0.1F;
    public float scaleSpeed = 0.0001F;

    public OnBoolValuesChange onTouchDetected;
    private Vector3 mousePrevious;

    // Use this for initialization
    void Start () {
        touchArea = GetComponent<RectTransform>();
	}

    
    void Update() {

        Canvas canvas = touchArea.GetComponentInParent<Canvas>();
        
        Rect area = new Rect( touchArea.anchoredPosition, touchArea.rect.size * canvas.scaleFactor);

        //Debug.Log(area);

        //Debug.Log(Input.touchCount > 0 ? Input.GetTouch(0).position : Vector2.zero);

        bool touchDetected = false;
        

        if (Input.touchCount > 1 && (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved) &&
            area.Contains(Input.GetTouch(0).position, true) && area.Contains(Input.GetTouch(1).position, true)) {

            Vector2 axis = Input.GetTouch(0).position - Input.GetTouch(1).position;
            Vector2 oldaxis = axis + Input.GetTouch(0).deltaPosition - Input.GetTouch(1).deltaPosition;
            oldaxis.Scale(new Vector2(-1, -1));
            axis.Scale(new Vector2(-1, -1));
            handle.transform.rotation *= Quaternion.FromToRotation(axis, oldaxis);
            touchDetected = true;
        }

        if (Input.touchCount > 1 && (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved) &&
            area.Contains(Input.GetTouch(0).position, true) && area.Contains(Input.GetTouch(1).position, true)) {

            Vector2 axis = Input.GetTouch(0).position - Input.GetTouch(1).position;
            Vector2 oldaxis = axis + Input.GetTouch(0).deltaPosition - Input.GetTouch(1).deltaPosition;

            float delta = (axis.magnitude - oldaxis.magnitude); 

            // Get movement of the finger since last frame
            handle.transform.localScale = handle.transform.localScale - new Vector3(delta * scaleSpeed, delta * scaleSpeed, 1);
            
            if(handle.transform.localScale.x <= 0.5f) {
                handle.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            }
            if (handle.transform.localScale.x >= 3f) {
                handle.transform.localScale = new Vector3(3f, 3f, 3f);
            }
            touchDetected = true;

        } else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved &&
            area.Contains(Input.GetTouch(0).position, true)) {
            // Get movement of the finger since last frame
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

            // Move object across XY plane
            handle.transform.Translate(touchDeltaPosition.x * speed, touchDeltaPosition.y * speed, 0);
            touchDetected = true;
        }
        //else if (Input.GetMouseButton(0) && area.Contains(Input.mousePosition, true)) {
        //    // Get movement of the finger since last frame
        //    Vector2 touchDeltaPosition = Input.mousePosition - mousePrevious;
        //    mousePrevious = Input.mousePosition;
        //    // Move object across XY plane
        //    handle.transform.Translate(touchDeltaPosition.x * speed, touchDeltaPosition.y * speed, 0);
        //    touchDetected = true;
        //} else {
        //    mousePrevious = Input.mousePosition;
        //}


            onTouchDetected.Invoke(touchDetected);
    }
}
