﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class NewModelsChecker : MonoBehaviour {

    public PictureContainer picture;
    public RawImage selectedModel;
    public RectTransform content;
    public ModelImageItem item;
    public UnityEngine.UI.ScrollRect scrollViewRect;

    private string serverPath = "ftp://ftp.zedsolegrau.com.br//private/modelos-app/";
    private string username = "f19denise";
    private string password = "zA9jqzBM_J";
    private string imagesPath;
    
    private ModelImageItem[] models;
    private List<string> filesInPath = new List<string>();
    private List<string> serverFiles = new List<string>();
    private string selectedModelPath = "";


    // Use this for initialization
    void Start () {

        
	}
	
    void OnEnable() {
#if UNITY_ANDROID && !UNITY_EDITOR
        imagesPath = Application.persistentDataPath + "/espelho virtual/images/";
#elif UNITY_EDITOR
        imagesPath = Application.dataPath + "/espelho virtual/images/";
#endif
        StartCoroutine(CheckDownload());
    }

    public void CancelDownload() {
        FtpDownload.Instance.Cancel();
    }

    IEnumerator CheckDownload() {

        yield return new WaitUntil(() => { return DownloadFromServer.Instance.Done; });

        PopupMessage.Instance.ShowPopup(true);
        PopupMessage.Instance.confirmButtom.onClick.AddListener(CancelDownload);
        Coroutine coroutine = StartCoroutine(LoadImageFiles());

        Debug.Log("Start request: " + System.DateTime.Now.Second);
        //Get file with list of models from server
        FtpDownload.Instance.downloadWithFTP(serverPath + "modelos.txt", "", username, password);
        while (!FtpDownload.Instance.Done && !FtpDownload.Instance.Failed) {
            yield return null; //TODO: make a timeout and get progress
        }
        Debug.Log("End request: " + System.DateTime.Now.Second);

        if (!FtpDownload.Instance.Failed) {
            string str = System.Text.Encoding.Default.GetString(FtpDownload.Instance.Bytes);
            serverFiles = new List<string>(System.Text.RegularExpressions.Regex.Split(str, "\r\n|\r|\n"));
            filesInPath = GetAllFileInDir();

            //Remove files from local directory which where removed from the server list
            foreach (string s in filesInPath) {
                if (!serverFiles.Contains(s)) {
                    RemoveFile(s);
                }
            }
        }

        //Download files from server which are new in the server list
        foreach (string s in serverFiles) {
            if (!filesInPath.Contains(s)) {
                FtpDownload.Instance.downloadWithFTP(serverPath + s, imagesPath + s, username, password);
                while (!FtpDownload.Instance.Done && !FtpDownload.Instance.Failed) {
                    yield return null; //TODO: make a timeout and get progress
                }
            }
            PopupMessage.Instance.SetProgress(((float)serverFiles.IndexOf(s) + 1)/(float)(serverFiles.Count));
        }
        PopupMessage.Instance.ShowPopup(false);
        PopupMessage.Instance.confirmButtom.onClick.RemoveListener(CancelDownload);

        StopCoroutine(coroutine);
        CleanAll();
        StartCoroutine(LoadImageFiles());
    }

    void OnDisable() {
        selectedModel.gameObject.SetActive(false);
        picture.gameObject.SetActive(false);
        selectedModelPath = "";
        CleanAll();
    }

    void CleanAll() {

        foreach (ModelImageItem m in models) {
            if (m != null && m.image != null) {
                DestroyImmediate(m.image.texture);
            }
            if (m != null) {
                Destroy(m.gameObject);
            }
        }
    }


    public IEnumerator LoadImageFiles() {

        filesInPath = GetAllFileInDir();
        models = new ModelImageItem[filesInPath.Count];
        
        RectTransform rectTransform = item.GetComponent<RectTransform>();
        content.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, filesInPath.Count * rectTransform.rect.width);
        float scrollRectWidth = scrollViewRect.GetComponent<RectTransform>().rect.width;


        //Select the previous model selected (in case it was updated)
        if (filesInPath.Count > 0 && selectedModel.texture == null) {
            int selectedIndex = selectedModelPath != "" ? filesInPath.IndexOf(selectedModelPath) : 0;

            Rect selectedPictureRect = new Rect(rectTransform.rect.width * selectedIndex, 0, rectTransform.rect.width, -rectTransform.rect.height);

            ModelImageItem selected = Instantiate(item, content.transform);
            selected.transform.position = content.transform.TransformPoint(selectedPictureRect.position);
            models[selectedIndex] = selected;
            models[selectedIndex].imageUri = filesInPath[selectedIndex];
            selected.onModelSelected += OnModelSelectedChange;
            DownloadFromServer.Instance.SetPicture(imagesPath + filesInPath[selectedIndex], false, models[selectedIndex].image);

            while (!DownloadFromServer.Instance.Done) {//TODO: And no failed
                yield return null; //TODO: make a timeout and get progress
            }

            OnModelSelectedChange(selected);
        }

        while (true) {
            
            float normhorizontal = scrollViewRect.horizontalNormalizedPosition;
            float currentPos = (content.rect.width - scrollRectWidth) * normhorizontal;
            Rect viewArea = new Rect(currentPos, content.rect.y, scrollRectWidth, -content.rect.height);

            for (int i = 0; i < models.Length; i++) {
                
                
                Rect pictureRect = new Rect(rectTransform.rect.width * i, 0, rectTransform.rect.width, -rectTransform.rect.height);
                
                //Assure the picture was not loaded yet and it is in the visible area, before loading
                if (models[i] == null && viewArea.Overlaps(pictureRect, true)) {
                    ModelImageItem m = Instantiate(item, content.transform);
                    m.transform.position = content.transform.TransformPoint(pictureRect.position);
                    models[i] = m;
                    models[i].imageUri = filesInPath[i];
                    m.onModelSelected += OnModelSelectedChange;
                    DownloadFromServer.Instance.SetPicture(imagesPath + filesInPath[i], false, models[i].image);
                    while (!DownloadFromServer.Instance.Done) {//TODO: And no failed
                        yield return null; //TODO: make a timeout and get progress
                    }

                } else if (models[i] != null && !viewArea.Overlaps(pictureRect, true) && models[i].image.texture != selectedModel.texture) { //don't destroy the selected model
                    DestroyImmediate(models[i].image.texture);
                    Destroy(models[i].gameObject);
                    models[i] = null;
                }
            }
            yield return null;
        }
    }

    private void OnModelSelectedChange(ModelImageItem item) {
        selectedModel.gameObject.SetActive(true);
        Vector2 imgSize = selectedModel.rectTransform.rect.size;
        //Adjust the image size to fit the thumbnail withouth overflowing the cell
        float texAspect = (float)item.image.texture.width / (float)item.image.texture.height;
        if (texAspect >= 1) {
            Debug.Log("texture resize: " + new Vector2((int)imgSize.x, (int)(imgSize.x / texAspect)));
            selectedModel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (int)imgSize.x);
            selectedModel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (int)(imgSize.x / texAspect));
        } else {
            Debug.Log("texture resize: " + new Vector2((int)(imgSize.y * texAspect), (int)imgSize.y));
            selectedModel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (int)(imgSize.y * texAspect));
            selectedModel.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (int)imgSize.y);
        }

        selectedModel.texture = item.image.texture;
        selectedModelPath = item.imageUri;
    }

    public void OnSetPicture(string uri) {
        if (picture.picture != null)
            DestroyImmediate(picture.picture.texture);

        picture.gameObject.SetActive(true);
        DownloadFromServer.Instance.SetPicture(uri, false, picture.picture);
    }

    void SaveImageFile(string name,  byte[] file) {
        FileStream filestream = new FileStream(imagesPath + name, FileMode.CreateNew, FileAccess.ReadWrite);
        filestream.Write(file, 0, file.Length);
        filestream.Flush();
        filestream.Close();
    }

    private void RemoveFile(string s) {
        File.Delete(imagesPath + s);
    }

    List<string> GetAllFileInDir() {
        List<string> filesInPath = new List<string>();
        string path = imagesPath;
        DirectoryInfo dataDir = new DirectoryInfo(path);
        try {
            FileInfo[] fileinfo = dataDir.GetFiles("*.png");
            for (int i = 0; i < fileinfo.Length; i++) {
                filesInPath.Add(fileinfo[i].Name);
            }
        } catch (Exception e) {
            Debug.Log(e);
        }
        return filesInPath;
    }
}
