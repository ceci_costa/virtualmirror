﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class AccessAndroidGallery : Singleton<AccessAndroidGallery> {

    internal int uriCount = 0;
    List<string> galleryImagesUri = new List<string>();
    private WWW loader;

    public WWW Loader {
        get {
            return loader;
        }
    }

    public List<string> GalleryImagesUri {
        get {
            return galleryImagesUri;
        }
    }

    public void LoadAllImageUri() {
        galleryImagesUri = GetAllImagePaths();
        uriCount = galleryImagesUri.Count;
    }

    //public void LoadImage(int index, RawImage img) {
    //    StartCoroutine(LoadImageBg(index, img));
    //}

    //private IEnumerator LoadImageBg(int index, RawImage img) {
    //    Texture2D t = new Texture2D(2, 2);
    //    loader = new WWW("file://" + galleryImagesUri[index]);
    //    yield return loader;
    //    loader.LoadImageIntoTexture(t);

    //    //Thread thread = new Thread( () => {
    //    //    Vector2 imgSize = img.rectTransform.rect.size;
    //    //    //Adjust the image size to fit the thumbnail withouth overflowing the cell
    //    //    float texAspect = (float)t.width / (float)t.height;
    //    //    if (texAspect >= 1) {
    //    //        Debug.Log("texture resize: " + new Vector2((int)imgSize.x, (int)(imgSize.x / texAspect)));
    //    //        TextureScale.Bilinear(t, (int)imgSize.x, (int)(imgSize.x / texAspect));
    //    //    } else {
    //    //        Debug.Log("texture resize: " + new Vector2((int)(imgSize.y * texAspect), (int)imgSize.y));
    //    //        TextureScale.Bilinear(t, (int)(imgSize.y * texAspect), (int)imgSize.y);
    //    //    }
    //    //    img.texture = t;
    //    //    img.SetNativeSize();
    //    //});
    //    //thread.Start();

    //    Vector2 imgSize = img.rectTransform.rect.size;
    //    //Adjust the image size to fit the thumbnail withouth overflowing the cell
    //    float texAspect = (float)t.width / (float)t.height;
    //    if (texAspect >= 1) {
    //        img.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (int)imgSize.x);
    //        img.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (int)(imgSize.x / texAspect));
    //    } else {
    //        img.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (int)(imgSize.y * texAspect));
    //        img.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (int)imgSize.y);
    //    }
    //    img.texture = t;
    //}

    private List<string> GetAllImagePaths() {
#if UNITY_ANDROID && !UNITY_EDITOR
    return GetAllGalleryImagePaths();
#elif UNITY_EDITOR
        return GetAllFileInDir();
#endif
    }

    private List<string> GetAllGalleryImagePaths() {
        List<string> results = new List<string>();
        HashSet<string> allowedExtesions = new HashSet<string>() { ".png", ".jpg", ".jpeg" };

        try {
            AndroidJavaClass mediaClass = new AndroidJavaClass("android.provider.MediaStore$Images$Media");

            // Set the tags for the data we want about each image.  This should really be done by calling; 
            //string dataTag = mediaClass.GetStatic<string>("DATA");
            // but I couldn't get that to work...

            const string dataTag = "_data";

            string[] projection = new string[] { dataTag };
            AndroidJavaClass player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = player.GetStatic<AndroidJavaObject>("currentActivity");

            string[] urisToSearch = new string[] { "EXTERNAL_CONTENT_URI", "INTERNAL_CONTENT_URI" };
            foreach (string uriToSearch in urisToSearch) {
                AndroidJavaObject externalUri = mediaClass.GetStatic<AndroidJavaObject>(uriToSearch);
                AndroidJavaObject finder = currentActivity.Call<AndroidJavaObject>("managedQuery", externalUri, projection, null, null, null);
                bool foundOne = finder.Call<bool>("moveToFirst");
                while (foundOne) {
                    int dataIndex = finder.Call<int>("getColumnIndex", dataTag);
                    string data = finder.Call<string>("getString", dataIndex);
                    if (allowedExtesions.Contains(Path.GetExtension(data).ToLower())) {
                        string path =  data;
                        Debug.Log(data);
                        results.Add(path);
                    }

                    foundOne = finder.Call<bool>("moveToNext");
                }
            }
        } catch (System.Exception e) {
            Debug.LogError(e.Message);
        }

        return results; 
    }

    List<string> GetAllFileInDir() {
        List<string> filesInPath = new List<string>();
        string path = "Assets/";
        DirectoryInfo dataDir = new DirectoryInfo(path);
        try {
            //FileInfo[] fileinfo = dataDir.GetFiles("*.png;*.jpg", SearchOption.AllDirectories);

            string[] extensions = { "jpg", "png", "jpeg"};

            FileInfo[] fileinfo = dataDir.GetFiles("*.*", SearchOption.AllDirectories)
                .Where(f => extensions.Contains(f.FullName.Split('.').Last().ToLower())).ToArray();

            for (int i = 0; i < fileinfo.Length; i++) {
                filesInPath.Add(fileinfo[i].Directory + "\\" + fileinfo[i].Name);
            }
        } catch (System.Exception e) {
            Debug.Log(e);
        }
        return filesInPath;
    }
}