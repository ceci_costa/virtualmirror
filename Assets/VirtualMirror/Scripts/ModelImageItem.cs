﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModelImageItem : MonoBehaviour {

    [SerializeField]
    public RawImage image;
    public bool loaded = false;

    public delegate void OnModelSelected(ModelImageItem item);
    public OnModelSelected onModelSelected;
    internal string imageUri;

    public void OnSelectModel() {
        onModelSelected(this);
    }
}
