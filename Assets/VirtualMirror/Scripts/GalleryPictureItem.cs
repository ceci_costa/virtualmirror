﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GalleryPictureItem : MonoBehaviour {

    public RawImage picture;
    internal bool loaded = false;
    internal string uri;

    public delegate void OnPictureSelected(string uri);
    public OnPictureSelected onPictureSelected;

    public void RecalculateRectSize(float scaleFactor = 1) {
        RectTransform layout = GetComponent<RectTransform>();
        if (layout != null) {
            float aspect = (float)Screen.width / (float)Screen.height;
            Vector2 size = layout.rect.size;
            size.x = Screen.width / 2 - 5;
            size.y = size.x / aspect;
            layout.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x / scaleFactor);
            layout.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y / scaleFactor);
        }
    }

    public void OnSelectPicture() {
        onPictureSelected(uri);
    }
}