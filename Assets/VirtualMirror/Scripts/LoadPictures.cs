﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class LoadPictures : MonoBehaviour {
    [System.Serializable]
    public class SelectPictureEvent : UnityEvent<string> { }

    public GameObject container;
    public ScrollRect scrollViewRect;
    public GalleryPictureItem pictureItemReference;
    public Texture2D defaultPicture;

    private GalleryPictureItem[] pictures;
    private RectTransform scrollViewContent;
    
    private float scrollViewRectHeight;
    private List<GalleryPictureItem> loaded = new List<GalleryPictureItem>();
    private Coroutine updater;

    [SerializeField]
    public SelectPictureEvent onPictureSelected;

    // Use this for initialization
    void Start () {
        scrollViewContent = scrollViewRect.content.GetComponent<RectTransform>();
        scrollViewRectHeight = scrollViewRect.GetComponent<RectTransform>().rect.height;
        pictureItemReference.RecalculateRectSize(GetComponentInParent<Canvas>().scaleFactor);
    }

    public void Open() {
        container.SetActive(true);
        updater = StartCoroutine(UpdatePictures());
    }

    public void Close() {
        container.SetActive(false);
        StopCoroutine(updater);
        DeletePictureItems();
    }

    private void DeletePictureItems() {
        foreach (GalleryPictureItem gpi in loaded) {
            DestroyImmediate(gpi.picture.texture);
            Destroy(gpi.gameObject);
        }
        loaded.Clear();
    }

    public void OnSelectPicture(string uri) {
        onPictureSelected.Invoke(uri);
        Close();
    }

    // Update is called once per frame
    IEnumerator UpdatePictures () {
        AccessAndroidGallery.Instance.LoadAllImageUri();
        int countPics = AccessAndroidGallery.Instance.uriCount;
        RectTransform rectTransform = pictureItemReference.GetComponent<RectTransform>();
        int picsPerLine = (int)(scrollViewContent.rect.width / rectTransform.rect.size.x);
        scrollViewContent.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, rectTransform.rect.size.y * (countPics / picsPerLine));

        pictures = new GalleryPictureItem[countPics];

        List<string> gallerypaths = AccessAndroidGallery.Instance.GalleryImagesUri;
        
        while (true) {
            float normvertical = scrollViewRect.verticalNormalizedPosition - 1;
            float currentPos = (scrollViewContent.rect.height - scrollViewRectHeight) * normvertical;
            Rect viewArea = new Rect(scrollViewContent.rect.x, currentPos, scrollViewContent.rect.width, -scrollViewRectHeight);
            //Debug.Log("Loaded pics: " + countPics);
            //Debug.Log("view area: " + viewArea);

            int startingIndex = Mathf.FloorToInt(Mathf.Abs(currentPos / rectTransform.rect.height)) * picsPerLine;

            loaded.RemoveAll((g) => {
                RectTransform rect = g.GetComponent<RectTransform>();
                Vector2 position = scrollViewContent.transform.InverseTransformPoint(rect.position);
                Rect pictureRect = new Rect(position.x, position.y, rect.rect.width, -rect.rect.height);
                int posInferedIndex = Mathf.FloorToInt(Mathf.Abs(position.y/ rectTransform.rect.height));
                if (g != null && !viewArea.Overlaps(pictureRect, true)) {
                    DestroyImmediate(g.picture.texture);
                    Destroy(g.gameObject);
                    return true;
                }
                return false;
            });

            //foreach (GalleryPictureItem g in loaded) {

            //    RectTransform rect = g.GetComponent<RectTransform>();
            //    Rect pictureRect = new Rect(rect.position.x, rect.position.y, rect.rect.width, -rect.rect.height);
            //    int posInferedIndex = Mathf.FloorToInt(Mathf.Abs(rect.rect.y / rectTransform.rect.height));
            //    if (g != null && !viewArea.Overlaps(pictureRect, true)) {
            //        Destroy(g.picture.texture);
            //        Destroy(g.gameObject);
            //        pictures[posInferedIndex] = null;
            //    }
            //}
            //loaded.Clear();
            //Debug.Log("starting index: " + startingIndex);
            for (int i = startingIndex; i < pictures.Length; i++) {    
                Rect pictureRect = new Rect(rectTransform.rect.width * (i % 2), -rectTransform.rect.height * (i/picsPerLine), rectTransform.rect.width, -rectTransform.rect.height);

                //Debug.Log("picure rect " + i + ": " + pictureRect);

                if(!viewArea.Overlaps(pictureRect, true)) {
                    //Debug.Log("Nothing");
                    break;
                }

                //Assure the picture was not loaded yet and it is in the visible area, before loading
                if (pictures[i] == null) {
                    GalleryPictureItem picture = Instantiate(pictureItemReference, scrollViewContent.transform);
                    picture.transform.position = scrollViewContent.transform.TransformPoint(pictureRect.position);
                    picture.onPictureSelected += OnSelectPicture;
                    picture.uri = gallerypaths[i];
                    pictures[i] = picture;
                    DownloadFromServer.Instance.SetPicture(gallerypaths[i], false, pictures[i].picture);

                    yield return new WaitUntil(() => { return DownloadFromServer.Instance.GetProgress() >= 1; });


                    loaded.Add(picture);
                    Debug.Log("Loaded");
                } 

                yield return null;
            }
            yield return null;
        }
	}
}