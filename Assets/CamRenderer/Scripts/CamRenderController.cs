﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MarkerBasedARExample;
using System;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif

namespace VR
{
    /// <summary>
    /// GoogleVR With OpenCVForUnity sample.
    /// </summary>
    [RequireComponent(typeof(WebCamTextureToMatHelper))]
    public class CamRenderController : Singleton<CamRenderController> {
        
        public Camera ARCamera;

        public UnityEngine.UI.RawImage renderImage;
        WebCamTextureToMatHelper webCamTextureToMatHelper;
       
        public bool flipHorizontal;
        public bool flipVertical;

        public UnityEvent onPrepearForTakeScreenshot;
        public UnityEvent onFinishedTakingScreenshot;

        private Quaternion baseRotation;
        private Vector3 baseScale;

        public Button[] backButtons;

        public bool Freeze {
            get {
                return !webCamTextureToMatHelper.GetWebCamTexture().isPlaying;
            }
            set {
                if (value)
                    webCamTextureToMatHelper.GetWebCamTexture().Pause();
                else
                    webCamTextureToMatHelper.GetWebCamTexture().Play();
            }
        }

        private void Awake() {
            Screen.fullScreen = false;
        }

        /// <summary>
        /// Start this instance.
        /// </summary>
        void Start() {
            baseRotation = transform.rotation;
            baseScale = renderImage.transform.localScale;

            webCamTextureToMatHelper = gameObject.GetComponent<WebCamTextureToMatHelper>();
            webCamTextureToMatHelper.Init();
        }

        public void UpdateFrame() {
            
        }

        /// <summary>
        /// Raises the web cam texture to mat helper inited event.
        /// </summary>
        public void OnWebCamTextureToMatHelperInited() {

            WebCamTexture webCamTexture = webCamTextureToMatHelper.GetWebCamTexture();
            //gameObject.GetComponent<Renderer>().material.mainTexture = webCamTexture;

            Vector2 imgSize = renderImage.rectTransform.rect.size;

            Debug.Log("Texture angle: " + webCamTexture.videoRotationAngle);
            
            renderImage.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, webCamTexture.width);
            renderImage.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, webCamTexture.height);
            
            renderImage.texture = webCamTexture;

            Flip(); 
        }
        

        /// <summary>
        /// Raises the web cam texture to mat helper disposed event.
        /// </summary>
        public void OnWebCamTextureToMatHelperDisposed() {
            //			Debug.Log ("OnWebCamTextureToMatHelperDisposed");
        }

        void Update() {
            WebCamTexture webCamTexture = webCamTextureToMatHelper.GetWebCamTexture();
            renderImage.transform.rotation = baseRotation * Quaternion.AngleAxis(-webCamTexture.videoRotationAngle, Vector3.forward);

            
            if (Input.GetKeyDown(KeyCode.Escape)) {
                bool someActive = false;
                for (int i=0; i< backButtons.Length; i++) {
                    Button obj = backButtons[i];
                    if (obj.gameObject.activeInHierarchy) {
                        someActive = true;
                        obj.onClick.Invoke();
                        break;
                    }
                }
                if (!someActive)
                    Application.Quit();
            }
        }

        /// <summary>
        /// Raises the disable event.
        /// </summary>
        void OnDisable ()
		{
            webCamTextureToMatHelper.Dispose ();
		}


		/// <summary>
		/// Raises the play button event.
		/// </summary>
		public void OnPlayButton ()
		{
			webCamTextureToMatHelper.Play ();
		}

		/// <summary>
		/// Raises the pause button event.
		/// </summary>
		public void OnPauseButton ()
		{
			webCamTextureToMatHelper.Pause ();
		}

		/// <summary>
		/// Raises the stop button event.
		/// </summary>
		public void OnStopButton ()
		{
			webCamTextureToMatHelper.Stop ();
		}

		/// <summary>
		/// Raises the change camera button event.
		/// </summary>
		public void OnChangeCameraButton ()
		{
			webCamTextureToMatHelper.Init (null, webCamTextureToMatHelper.requestWidth, webCamTextureToMatHelper.requestHeight, !webCamTextureToMatHelper.requestIsFrontFacing);
		}

		public Rect GetSize(){
			return new Rect (0, 0, webCamTextureToMatHelper.requestWidth, webCamTextureToMatHelper.requestHeight);
		}

        public void TakePicture() {
            StartCoroutine(TakePictureScreenshot());
            onPrepearForTakeScreenshot.Invoke();
        }

        IEnumerator TakePictureScreenshot() {
            yield return new WaitForEndOfFrame();
            Debug.Log("Start Take screenshot");
            Texture2D snap = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            snap.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            snap.Apply();
            byte[] bytes = snap.EncodeToJPG();
            Debug.Log(bytes.Length);
#if UNITY_ANDROID && !UNITY_EDITOR
            string date = DateTime.Now.ToString("yyyy-MM-dd_h.mm.ss");
            Debug.Log(date);
            SaveTextureToGallery.SaveImageToGallery(snap, date, "Imagem salva pelo aplicativo Espelho Virtual em: " + date);
#endif
            DestroyImmediate(snap);
            Debug.Log("Finish Take screenshot");
            onFinishedTakingScreenshot.Invoke();
        }


        ///// <summary>
        ///// Flips the mat.
        ///// </summary>
        ///// <param name="mat">Mat.</param>
        private void Flip() {
            renderImage.transform.localScale = baseScale;

            WebCamTexture webCamTexture = webCamTextureToMatHelper.GetWebCamTexture();
            int flipCode = int.MinValue;

            if (webCamTextureToMatHelper.GetWebCamDevice().isFrontFacing) {
                if (webCamTexture.videoRotationAngle == 0) {
                    flipCode = 1;
                } else if (webCamTexture.videoRotationAngle == 90) {
                    flipCode = 1;
                }
                if (webCamTexture.videoRotationAngle == 180) {
                    flipCode = 0;
                } else if (webCamTexture.videoRotationAngle == 270) {
                    flipCode = 0;
                }
            } else {
                if (webCamTexture.videoRotationAngle == 180) {
                    flipCode = -1;
                } else if (webCamTexture.videoRotationAngle == 270) {
                    flipCode = -1;
                }
            }

            if (flipVertical) {
                if (flipCode == int.MinValue) {
                    flipCode = 0;
                } else if (flipCode == 0) {
                    flipCode = int.MinValue;
                } else if (flipCode == 1) {
                    flipCode = -1;
                } else if (flipCode == -1) {
                    flipCode = 1;
                }
            }

            if (flipHorizontal) {
                if (flipCode == int.MinValue) {
                    flipCode = 1;
                } else if (flipCode == 0) {
                    flipCode = -1;
                } else if (flipCode == 1) {
                    flipCode = int.MinValue;
                } else if (flipCode == -1) {
                    flipCode = 0;
                }
            }

            if (flipCode > int.MinValue) {
                Vector2 s = renderImage.transform.localScale;
                renderImage.transform.localScale = flipCode == 0? new Vector3(s.x, s.y * -1): flipCode == 1? new Vector3(s.x * -1, s.y) : new Vector3(s.x* -1, s.y* -1);
            }
        }

        public void Resume() {
            Freeze = false;
        }

        public void Pause() {
            Freeze = true;
        }
	}
}
